<?php

use Faker\Factory as Faker;
use App\Models\VehicleType;
use App\Repositories\VehicleTypeRepository;

trait MakeVehicleTypeTrait
{
    /**
     * Create fake instance of VehicleType and save it in database
     *
     * @param array $vehicleTypeFields
     * @return VehicleType
     */
    public function makeVehicleType($vehicleTypeFields = [])
    {
        /** @var VehicleTypeRepository $vehicleTypeRepo */
        $vehicleTypeRepo = App::make(VehicleTypeRepository::class);
        $theme = $this->fakeVehicleTypeData($vehicleTypeFields);
        return $vehicleTypeRepo->create($theme);
    }

    /**
     * Get fake instance of VehicleType
     *
     * @param array $vehicleTypeFields
     * @return VehicleType
     */
    public function fakeVehicleType($vehicleTypeFields = [])
    {
        return new VehicleType($this->fakeVehicleTypeData($vehicleTypeFields));
    }

    /**
     * Get fake data of VehicleType
     *
     * @param array $postFields
     * @return array
     */
    public function fakeVehicleTypeData($vehicleTypeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $vehicleTypeFields);
    }
}
