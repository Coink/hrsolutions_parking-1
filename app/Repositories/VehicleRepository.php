<?php

namespace App\Repositories;

use App\Models\Vehicle;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VehicleRepository
 * @package App\Repositories
 * @version January 14, 2018, 5:10 pm UTC
 *
 * @method Vehicle findWithoutFail($id, $columns = ['*'])
 * @method Vehicle find($id, $columns = ['*'])
 * @method Vehicle first($columns = ['*'])
*/
class VehicleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'identification',
        'vehicle_type'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Vehicle::class;
    }
}
