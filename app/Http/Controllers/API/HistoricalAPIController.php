<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateHistoricalAPIRequest;
use App\Http\Requests\API\UpdateHistoricalAPIRequest;
use App\Models\Historical;
use App\Repositories\Criterias\FiltersCriteria;
use App\Repositories\Criterias\WithRelationshipsCriteria;
use App\Repositories\HistoricalRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Controller\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class HistoricalController
 * @package App\Http\Controllers\API
 */

class HistoricalAPIController extends AppBaseController
{
    /** @var  HistoricalRepository */
    private $historicalRepository;

    public function __construct(HistoricalRepository $historicalRepo)
    {
        $this->historicalRepository = $historicalRepo;
        $this->historicalRepository->pushCriteria(new WithRelationshipsCriteria(['vehicle.vehicle_type.rate']));
    }

    /**
     * Display a listing of the Historical.
     * GET|HEAD /historicals
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->historicalRepository->pushCriteria(new RequestCriteria($request));
        $this->historicalRepository->pushCriteria(new LimitOffsetCriteria($request));

        if ($request->has('filters')) {
            $this->historicalRepository->pushCriteria(new FiltersCriteria($request->get('filters')));
        }
        $historicals = $this->historicalRepository->paginate(50);

        return $this->sendResponse($historicals->toArray(), 'Historicals retrieved successfully');
    }

    /**
     * Store a newly created Historical in storage.
     * POST /historicals
     *
     * @param CreateHistoricalAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHistoricalAPIRequest $request)
    {
        $input = $request->all();
        $historicals = $this->historicalRepository->create($input);

        return $this->sendResponse($historicals->toArray(), 'Historical saved successfully');
    }

    /**
     * Display the specified Historical.
     * GET|HEAD /historicals/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Historical $historical */
        $historical = $this->historicalRepository->findWithoutFail($id);

        if (empty($historical)) {
            return $this->sendError('Historical not found');
        }

        return $this->sendResponse($historical->toArray(), 'Historical retrieved successfully');
    }

    /**
     * Update the specified Historical in storage.
     * PUT/PATCH /historicals/{id}
     *
     * @param  int $id
     * @param UpdateHistoricalAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHistoricalAPIRequest $request)
    {
        $input = $request->all();

        /** @var Historical $historical */
        $historical = $this->historicalRepository->findWithoutFail($id);

        if (empty($historical)) {
            return $this->sendError('Historical not found');
        }

        $historical = $this->historicalRepository->update($input, $id);

        return $this->sendResponse($historical->toArray(), 'Historical updated successfully');
    }

    /**
     * Remove the specified Historical from storage.
     * DELETE /historicals/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Historical $historical */
        $historical = $this->historicalRepository->findWithoutFail($id);

        if (empty($historical)) {
            return $this->sendError('Historical not found');
        }

        $historical->delete();

        return $this->sendResponse($id, 'Historical deleted successfully');
    }
}
